package Advent

import AdventUtils._

object DayOne
  def fuelModule(mass: Int): Int =
    val fuel = (mass / 3) - 2
    if (fuel > 0) fuel else 0

  def totalFuelRequirement(mods: List[Int]): Int =
    mods.map(fuelModule).sum

  def adjustedFuelModule(mass: Int): Int =
    val amounts: LazyList[Int] = iterateF(fuelModule)(mass)
    amounts.drop(1).takeWhile(_ > 0).sum

  def adjustedTotalFuelRequirement(mods: List[Int]): Int =
    mods.map(adjustedFuelModule).sum

  @main def runOne: Unit =
    val nums = readIntPerLine("data/day1.txt")
    val totalFuel = totalFuelRequirement(nums)
    val adjustedTotalFuel = adjustedTotalFuelRequirement(nums)
    println(s"partOne: ${totalFuel}")
    println(s"partTwo: ${adjustedTotalFuel}")
