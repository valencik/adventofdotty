package AdventUtils

import scala.io.Source
import scala.language.implicitConversions

def readIntPerLine(file: String): List[Int] =
  Source.fromFile(file).getLines.map(_.toInt).toList

def iterateF[A](f: A => A)(a: A): LazyList[A] =
    a #:: iterateF(f)(f(a))
