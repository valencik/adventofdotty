import org.junit.Test
import org.junit.Assert._
import Advent.DayOne._

class DayOneTests
  @Test def testFuelModule() =
    assertEquals(fuelModule(12), 2)
    assertEquals(fuelModule(14), 2)
    assertEquals(fuelModule(1969), 654)
    assertEquals(fuelModule(100756), 33583)

  @Test def testTotalFuelRequirement() =
    assertEquals(totalFuelRequirement(List(14, 12)), 4)

  @Test def testAdjustedFuelModule() =
    assertEquals(adjustedFuelModule(14), 2)
    assertEquals(adjustedFuelModule(1969), 966)
    assertEquals(adjustedFuelModule(100756), 50346)

  @Test def testAdjustedTotalFuelRequirement() =
    assertEquals(adjustedTotalFuelRequirement(List(14, 12)), 4)
    assertEquals(adjustedTotalFuelRequirement(List(1969, 100756)), 966 + 50346)
